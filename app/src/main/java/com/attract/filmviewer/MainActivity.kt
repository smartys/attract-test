package com.attract.filmviewer

import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.transition.TransitionInflater
import android.view.View
import com.attract.filmviewer.fragments.FilmDetailPageFragment
import com.attract.filmviewer.fragments.FilmListFragment

class MainActivity : AppCompatActivity(), FilmListFragment.FilmClickListener {

    companion object{
        const val DATA_BUNDLE_POS_KEY = "FILM_POS"
        const val TRANSITION_NAME = "TRANSITION"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportFragmentManager.beginTransaction()
            .replace(R.id.fragment_container, FilmListFragment.newInstance())
            .commit()
    }

    override fun onItemClicked(itemPosition: Int, transitionElement: View) {
        val bundle = Bundle()
        bundle.putInt(DATA_BUNDLE_POS_KEY, itemPosition)
        bundle.putString(TRANSITION_NAME, TRANSITION_NAME + itemPosition)
        val detailPageFragment = FilmDetailPageFragment.newInstance()
        detailPageFragment.arguments = bundle
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            detailPageFragment.sharedElementEnterTransition =
                TransitionInflater.from(this).inflateTransition(R.transition.film_transition)
            supportFragmentManager.beginTransaction()
                .replace(R.id.fragment_container, detailPageFragment)
                .addSharedElement(transitionElement, TRANSITION_NAME + itemPosition)
                .addToBackStack(FilmDetailPageFragment.TAG)
                .commit()
        } else {
            supportFragmentManager.beginTransaction()
                .replace(R.id.fragment_container, detailPageFragment)
                .addToBackStack(FilmDetailPageFragment.TAG)
                .commit()
        }
    }
}

