package com.attract.filmviewer.fragments

import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.attract.filmviewer.MainActivity.Companion.DATA_BUNDLE_POS_KEY
import com.attract.filmviewer.MainActivity.Companion.TRANSITION_NAME
import com.attract.filmviewer.R
import com.attract.filmviewer.utils.FilmDataLoader
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.film_detail_fragment_layout.*

class FilmDetailPageFragment : FilmViewerBaseFragment() {

    private var filmPosition: Int = 0
    private var transitionName: String = ""

    companion object {
        val TAG = "FilmDetailPageFragment"
        fun newInstance(): FilmDetailPageFragment = FilmDetailPageFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val fragementView = inflater.inflate(R.layout.film_detail_fragment_layout, container, false)
        if (arguments?.getInt(DATA_BUNDLE_POS_KEY) != null) {
            filmPosition = arguments!!.getInt(DATA_BUNDLE_POS_KEY)
        }
        if (arguments?.getString(TRANSITION_NAME) != null) {
            transitionName = arguments!!.getString(TRANSITION_NAME)
        }
        return fragementView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            film_detail_image.transitionName = transitionName
        }
        val currentFilm = FilmDataLoader.fetchFilmBy(filmPosition)
        film_detail_title.setText(currentFilm.filmName)
        film_detail_description.setText(currentFilm.filmDescription)

        FilmDataLoader.fetchImageBy(currentFilm.filmImageUrl)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({image ->
                film_detail_image.setImageBitmap(image)
            },
                { errorMsg ->
                    Log.d("Error", " errorMsg: " + errorMsg)
                })

    }

}