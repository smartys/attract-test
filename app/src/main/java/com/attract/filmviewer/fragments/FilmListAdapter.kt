package com.attract.filmviewer.fragments

import android.content.Context
import android.os.Build
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import com.attract.filmviewer.R
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import android.util.Log
import android.widget.ImageView
import com.attract.filmviewer.MainActivity.Companion.TRANSITION_NAME
import com.attract.filmviewer.api.data.Film
import com.attract.filmviewer.utils.FilmDataLoader
import java.text.SimpleDateFormat
import java.util.*


class FilmListAdapter(private var list: List<Film>, private val clickListener: FilmListFragment.FilmClickListener)
    : RecyclerView.Adapter<FilmViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: kotlin.Int): FilmViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return FilmViewHolder(inflater, parent)
    }

    override fun onBindViewHolder(holder: FilmViewHolder, position: kotlin.Int) {
        val film: Film = list[position]
        holder.bind(film, position, clickListener)
    }

    override fun getItemCount(): kotlin.Int = list.size

    fun update(filmList: List<Film>) {
        list = filmList
        notifyDataSetChanged();
    }

}

class FilmViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
    RecyclerView.ViewHolder(inflater.inflate(R.layout.film_list_item, parent, false)) {
    private var mTitleView: TextView? = null
    private var mTimeView: TextView? = null
    private var mImageView: ImageView? = null


    init {
        mTitleView = itemView.findViewById(R.id.film_title)
        mTimeView = itemView.findViewById(R.id.film_time)
        mImageView = itemView.findViewById(R.id.film_image)
    }

    fun bind(film: Film, filmPosition: Int, callback: FilmListFragment.FilmClickListener) {
        mImageView!!.setImageResource(R.drawable.ic_placeholder)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mImageView!!.transitionName = TRANSITION_NAME + filmPosition
        }
        mTitleView?.text = film.filmName
        val dateSimpleFormatter = SimpleDateFormat("dd-MMM-YYYY HH:mm")
        val filmDate = dateSimpleFormatter.format(Date(film.filmTimeInUdefined))
        mTimeView?.text = filmDate
        FilmDataLoader.fetchImageBy(film.filmImageUrl)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ bitmap ->
                mImageView!!.setImageBitmap(bitmap)
            },
                {
                    Log.d("Error", "Unable load image")
                })
        mImageView!!.setOnClickListener { callback.onItemClicked(filmPosition, mImageView!!) }
    }

}