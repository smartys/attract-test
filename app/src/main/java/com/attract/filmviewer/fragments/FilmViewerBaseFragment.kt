package com.attract.filmviewer.fragments

import android.support.v4.app.Fragment
import io.reactivex.disposables.CompositeDisposable

open class FilmViewerBaseFragment : Fragment() {
    protected var disposables: CompositeDisposable = CompositeDisposable()

    override fun onPause() {
        super.onPause()
        disposables.dispose()
    }

}