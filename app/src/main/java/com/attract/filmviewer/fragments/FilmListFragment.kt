package com.attract.filmviewer.fragments

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.attract.filmviewer.R
import com.attract.filmviewer.api.FilmListAPI
import com.attract.filmviewer.utils.FilmDataLoader
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.film_list_fragment_layout.*
import java.lang.ClassCastException

class FilmListFragment : FilmViewerBaseFragment() {
    private lateinit var mCallback: FilmClickListener

    companion object {
        val TAG = "FilmListFragment"
        fun newInstance(): FilmListFragment = FilmListFragment()
    }

    interface FilmClickListener {
        fun onItemClicked(itemPosition: Int, transitionElement: View)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        try {
            mCallback = context as FilmClickListener
        } catch (e: ClassCastException) {
            throw ClassCastException(activity.toString()
                    + " must implement FilmClickListener")
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.film_list_fragment_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val filmListAdapter = FilmListAdapter(emptyList(), mCallback)
        film_recycler_view.apply {
            layoutManager = GridLayoutManager(activity, 2)
            adapter = filmListAdapter
            addItemDecoration(FilmGridItemDecorator(10, 2))
        }
        disposables.add(FilmListAPI.getFilmList()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe (
                { result ->
                    filmListAdapter.update(result)
                    FilmDataLoader.initLoader(result)
                },
                { error ->
                    Log.e("Error", "### err: " + error)
                }
            )
        )

    }

}