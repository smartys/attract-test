package com.attract.filmviewer.api

import com.attract.filmviewer.api.data.Film
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface FilmAPIService {

    @GET("test.json")
    fun getFilms(): Observable<List<Film>>

    companion object {
        fun create(): FilmAPIService {

            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://test.php-cd.attractgroup.com/")
                .build()

            return retrofit.create(FilmAPIService::class.java)
        }
    }
}