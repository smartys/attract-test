package com.attract.filmviewer.api.data

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.attract.filmviewer.db.DBConstants
import com.google.gson.annotations.SerializedName
import kotlin.Int

@Entity(tableName = DBConstants.FILMS_TABLE_NAME)
data class Film (

    @PrimaryKey(autoGenerate = false)
    @SerializedName("itemId")
    @ColumnInfo(name = DBConstants.FILMS_TABLE_ITEM_ID)
    val filmId: Int,

    @SerializedName("name")
    @ColumnInfo(name = DBConstants.FILMS_TABLE_ITEM_NAME)
    val filmName: String,

    @SerializedName("image")
    @ColumnInfo(name = DBConstants.FILMS_TABLE_ITEM_IMAGE_URL)
    val filmImageUrl: String,

    @SerializedName("description")
    @ColumnInfo(name = DBConstants.FILMS_TABLE_ITEM_DESCRIPTION)
    val filmDescription: String,

    @SerializedName("time")
    @ColumnInfo(name = DBConstants.FILMS_TABLE_ITEM_TIME)
    val filmTimeInUdefined: Long
)