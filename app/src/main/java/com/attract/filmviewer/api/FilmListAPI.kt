package com.attract.filmviewer.api

import com.attract.filmviewer.api.data.Film
import io.reactivex.Observable
import io.reactivex.disposables.Disposable

object FilmListAPI {

    private val filmApiService by lazy {
        FilmAPIService.create()
    }

    private lateinit var mDisposable: Disposable

    fun getFilmList(): Observable<List<Film>> {
        return filmApiService.getFilms()
    }

}