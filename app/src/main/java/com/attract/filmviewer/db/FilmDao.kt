package com.attract.filmviewer.db

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.attract.filmviewer.api.data.Film

@Dao
interface FilmDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(items: List<Film>)

    @Query("SELECT * FROM " + DBConstants.FILMS_TABLE_NAME)
    fun getAll(): List<Film>

}