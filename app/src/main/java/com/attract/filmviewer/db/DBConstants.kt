package com.attract.filmviewer.db

interface DBConstants {

    companion object {
        const val DB_NAME = "database_films"
        const val FILMS_TABLE_NAME = "films"

        const val FILMS_TABLE_ITEM_ID = "films_item_id"
        const val FILMS_TABLE_ITEM_NAME = "films_item_name"
        const val FILMS_TABLE_ITEM_IMAGE_URL = "films_item_image_url"
        const val FILMS_TABLE_ITEM_DESCRIPTION = "films_item_description"
        const val FILMS_TABLE_ITEM_TIME = "films_item_time"

    }

}