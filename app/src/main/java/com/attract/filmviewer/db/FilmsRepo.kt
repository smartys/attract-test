package com.attract.filmviewer.db

import com.attract.filmviewer.api.data.Film
import io.reactivex.Observable

class FilmsRepo (private val filmDao: FilmDao) {

    fun getFilms():Observable<List<Film>> {
        return Observable.just(filmDao.getAll())
    }
}