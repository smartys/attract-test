package com.attract.filmviewer.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import com.attract.filmviewer.api.data.Film

@Database(entities = arrayOf(Film :: class), version = 1, exportSchema = false)
abstract class FilmsDB : RoomDatabase() {

    abstract fun filmDao(): FilmDao

    companion object {
        @Volatile
        private var INSTANCE: FilmsDB? = null

        fun getDatabase(context: Context): FilmsDB {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(context.applicationContext,
                    FilmsDB::class.java,
                    DBConstants.DB_NAME)
                    .build()
                INSTANCE = instance
                return instance
            }
        }
    }
}