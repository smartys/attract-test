package com.attract.filmviewer.utils

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Log
import com.attract.filmviewer.api.data.Film
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.io.InputStream
import java.net.URL

object FilmDataLoader {

    private val imageCache: HashMap<String, Bitmap> = HashMap()
    private var filmData: List<Film> = emptyList()
    private var disposables: Disposable? = null

    fun initLoader(films: List<Film>) {
        filmData = films
        if (!films.isEmpty()) {
            startImageCaching()
        }
    }

    fun fetchImageBy(imageUrl: String): Observable<Bitmap> {
        if (imageCache.containsKey(imageUrl)) {
            return Observable.just(imageCache.get(imageUrl)!!)
        } else {
            return createImageObservable(imageUrl)
        }
    }

    fun fetchFilmBy(position: kotlin.Int): Film {
        return filmData.get(position)
    }

    fun disposeDataLoader() {
        disposables?.dispose()
    }

    private fun startImageCaching() {
        for (film in filmData) {
            disposables =
            createImageObservable(film.filmImageUrl)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ imageResult ->
                    imageCache.put(film.filmImageUrl, imageResult)
                },
                    {
                        Log.d("Error", "errMsg: " + it)
                    })
        }
    }

    private fun createImageObservable(imageUrl: String): Observable<Bitmap> {
        return Observable.just(imageUrl)
            .flatMap {
                var inputImageStream: InputStream = URL(it).getContent() as InputStream
                val bitmap =
                    BitmapFactory.decodeStream(inputImageStream)
                inputImageStream.close()
                return@flatMap Observable.just(bitmap)
            }

    }

}